if &cp | set nocp | endif

filetype off
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'plasticboy/vim-markdown'
Plugin 'majutsushi/tagbar'
Plugin 'ervandew/supertab'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-scripts/a.vim'
Plugin 'sjl/gundo.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
Plugin 'chriskempson/base16-vim'
Plugin 'tpope/vim-surround'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'tpope/vim-repeat'
Plugin 'nathanaelkane/vim-indent-guides'

" Python bits
Plugin 'hynek/vim-python-pep8-indent'
Plugin 'nvie/vim-flake8'
Plugin 'Glench/Vim-Jinja2-Syntax'

" Docker bits
Plugin 'ekalinin/Dockerfile.vim'

" Autocompletion
Plugin 'Valloric/YouCompleteMe'

call vundle#end()
filetype plugin indent on

" Markdown autodetect is (was?) iffy
autocmd BufEnter *.md set ft=markdown

" Read in a skeleton file for new python files
autocmd BufNewFile *.py 0r ~/.vim/skel.py
autocmd BufNewFile *.py normal! G"_ddgg

" Flake8 on save
autocmd BufWritePost *.py call Flake8()

" Grep opens in pane
autocmd QuickFixCmdPost *grep* cwindow

" HTML doesn't need four space tabs
autocmd BufEnter *.html set shiftwidth=2
autocmd BufEnter *.html set tabstop=2
autocmd BufEnter *.html set softtabstop=2

" tab navigation like firefox
nmap <C-S-tab> :bprevious<CR>
nmap <C-tab> :bnext<CR>
map <C-S-tab> :bprevious<CR>
map <C-tab> :bnext<CR>
imap <C-S-tab> <Esc>:bprevious<CR>i
imap <C-tab> <Esc>:bnext<CR>i
nmap <C-t> :e .<CR>
imap <C-t> <Esc>:e .<CR>
nmap <C-w> :bdelete <CR>
imap <C-t> <Esc>:bdelete <CR>

" Split window navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Some fugitive commands
map <Leader>s :Gstatus<CR>
map <Leader>u :Gpush<CR>
" from old scumm games, pull = y
map <Leader>y :Gpull<CR>
map <Leader>d :Gdiff<CR>
" No <CR> because we'll want to add parameters
map <Leader>c :Git checkout 
map <Leader>m :Gmerge 
map <Leader>b :Git branch 

" Gundo
map <Leader>gu :GundoToggle<CR>

" Taglist
map <Leader>t :TagbarToggle<CR>

" a.vim
map <Leader>a :A<CR>

" Refresh CtrlP
map <Leader>r :CtrlPClearAllCaches<CR>
map <C-b> :CtrlPBuffer<CR>

" Word count!
map <Leader>wc :w !wc<CR>

" Clear search
map <Leader>x :noh<CR>

nnoremap <Space> :

map! <S-Insert> <MiddleMouse>
nmap gx <Plug>NetrwBrowseX
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
map <S-Insert> <MiddleMouse>
noremap j gj
noremap k gk

syntax on
syntax sync fromstart

set autoindent
set background=dark
set backspace=indent,eol,start
set backupdir=~/.vimbackup,.,/tmp
set clipboard+=unnamed  " Yanks go on clipboard instead.
set completeopt=menuone,longest
set cpo&vim
set cursorline
set diffopt+=vertical
set directory=~/.vimtmp,/tmp
set encoding=utf8 " default to UTF-8 encoding
set expandtab
set fileencoding=utf8
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set hidden
set history=50
set hlsearch " highlight all matches
set ignorecase
set incsearch " incremental searching on
set laststatus=2
set list
set listchars=tab:»·,trail:·,precedes:<,extends:> " enable visible whitespace
set mouse=a
set number
set relativenumber
set ruler
set scrolloff=5
set selectmode=key
set shiftwidth=4
set showmatch " Show matching braces.
set showtabline=2 " show always for console version
set smartcase
set smarttab
set softtabstop=4
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set t_Co=256
set tabstop=4
set termencoding=utf-8
set textwidth=120
set timeoutlen=1000
set ttimeoutlen=10
set undodir=~/.vimundo
set undofile
set virtualedit=block
set wildchar=9 " tab as completion character
set wildignore=*.o,*.hi,*.old,*~,*.svg,*.pyc,*.swp
set wildmenu " menu on statusbar for command autocomplete
set window=47

if has("gui_running")
  set autochdir
  set guifont=Ubuntu\ Mono\ Light\ 11
  set guioptions=agi
endif

let $PAGER=''
let $PATH=$PATH.":/home/kit/.cabal/bin"
let base16colorspace=256  " Access colors present in 256 colorspace

" Plugin configurations
let g:SuperTabDefaultCompletionType = "context"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 0
let g:airline_powerline_fonts = 1
let g:airline_theme='dark'
let g:ctrlp_custom_ignore = '\.git$'
let g:ctrlp_extensions = ['dir']
let g:ctrlp_open_new_file = 'r'
let g:molokai_original=1
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

" Builtin g: variables
let g:html_indent_inctags = "html,body,head,tbody"

let s:cpo_save=&cpo
let &cpo=s:cpo_save
unlet s:cpo_save

" Add support for markdown files in tagbar.
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '~/.vim/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes',
    \ 'kinds' : [
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
\ }

colorscheme base16-monokai
