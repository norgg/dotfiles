#!/bin/bash -eu
set -o pipefail
git clone https://bitbucket.org/KitB/dotfiles.git $HOME/.dotfiles

pushd $HOME

touch .bashrc.local

for f in .dotfiles/*
do
    if [ $f != .dotfiles/setup.sh ]
    then
        mv ".$(basename $f)" ".$(basename $f).old" || true
        ln -s "$f" ".$(basename $f)"
    fi
done

git clone https://github.com/gmarik/Vundle.vim.git $(readlink -f $HOME/.vim)/bundle/Vundle.vim
mkdir -p $HOME/.vim/colors
curl https://raw.githubusercontent.com/tomasr/molokai/master/colors/molokai.vim > $HOME/.vim/colors/molokai.vim

vim -c 'PluginInstall' -c 'qa!'
pushd .vim/bundle/YouCompleteMe/
./install.sh
popd

popd
