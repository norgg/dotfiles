# Check for an interactive session
[ -z "$PS1" ] && return

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

alias ls='ls --color=auto'
alias la='ls -la'
alias lah='ls -lah'
alias grep='grep --color=auto -i'
alias can-has='sudo apt-get install'
alias pyongyang="ping"
alias typecheck="ghc-mod check"
alias sprunge="curl -F 'sprunge=<-' http://sprunge.us"
alias last="PAGER=less git lg"
alias xm='setxkbmap gb; xmodmap -e "clear Lock" && xmodmap -e "keysym Caps_Lock = Escape"'
alias suod="sudo"
alias mouse_setup="sudo ~/Programming/C/logitech-mx-performance-dpi/performance_mx_dpi 16"

export NLTK_DATA=/usr/share/nltk/data/
export EDITOR=vim
export PATH=$PATH:~/.bin
export PYTHONPATH=$PYTHONPATH:.

set -o vi

PS1='(\A) [\[\e[0;32m\]\u\[\e[m\]@\[\e[0;33m\]\h\[\e[m\] \[\e[1;34m\]\W\[\e[m\]]\$ '
source ~/.bashrc.local
source ~/.colours.sh
cowsay butts
